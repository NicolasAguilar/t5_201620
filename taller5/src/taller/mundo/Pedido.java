package taller.mundo;

public class Pedido implements Comparable
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;

	/**
	 * Autor del pedido
	 */
	private String autorPedido;

	/**
	 * Cercania del pedido
	 */
	private int cercania;

	// ----------------------------------
	// Constructor
	// ----------------------------------

	/**
	 * Constructor del pedido
	 * TODO Defina el constructor de la clase
	 */
	public Pedido (double pre, String clie, int cerc)
	{
		autorPedido = clie;
		precio = pre;
		cercania = cerc;
	}

	// ----------------------------------
	// Métodos
	// ----------------------------------

	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}

	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}

	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}

	// TODO 
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return (int) (precio - ((Pedido)o).getPrecio());
	}


}
