package taller.estructuras;

public class Heap<T extends Comparable> implements IHeap<T>{

	public final static  int TAM = 20;
	private T[] pq;
	private int N = 0;
	public Heap()
	{
		pq = (T[])new Comparable[TAM];		
	}
	@Override
	public void add(T elemento) {
		// TODO Auto-generated method stub
		pq[++N] = elemento;
		N++;
		siftUp();
	}

	@Override
	public T peek() {
		// TODO Auto-generated method stub
		return (T)pq[1];
	}
	
	@Override
	public T poll() {
		// TODO Auto-generated method stub
		T max =(T) pq[1]; // Retrieve max key from top.
		exch(1, N--); // Exchange with last item.
		pq[N+1] = null; // Avoid loitering.
		siftDown(); // Restore heap property.
		return max;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return N;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return N == 0;
	}

	@Override
	public void siftUp() {
		// TODO Auto-generated method stub
		int k = N;
		while (k > 1 && less(k/2, k) && pq[k/2]!=null && pq[k]!=null)
		{
			exch(k/2, k);
			k = k/2;
		}
	}

	@Override
	public void siftDown() {
		// TODO Auto-generated method stub
		int k = 1;
		while (2*k <= N)
		{
			int j = 2*k;
			if (j < N && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

	private void exch(int i, int j)
	{ T t =(T) pq[i]; pq[i] = pq[j]; pq[j] = t; }

	private boolean less(int i, int j)
	{ 
			return ((T)pq[i]).compareTo((T)pq[j]) < 0; 
		
	}
}
